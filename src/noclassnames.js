export default function (...classes) {
    classes = classes || [];

    let output = [];
    for (let i = 0; i < classes.length; i++) {
        let cl = classes[i];
        if (Array.isArray(cl)) {
            classes.push(...cl);
        } else if (typeof cl === "object") {
            let keys = Object.keys(cl);
            output.push(...(keys.map(v => !!(cl[v]))));
        } else if (!!cl)
            output.push(cl.toString());
    }

    return output.join(" ");
}