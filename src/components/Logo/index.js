import React from "react";
import s from "./style.module.css";
import { ReactComponent as ReactSvgLogo } from "../../logo.svg";

const Logo = ({ color = "#61DAFB" }) => (
    <ReactSvgLogo style={ { "--logo-color": color } } className={ s.logo }/>
);

export default Logo;