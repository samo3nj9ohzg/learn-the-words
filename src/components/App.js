import React from "react";

import Logo from "./Logo";
import Header from "./Header";
import CardList from "./CardList";
import Paragraph from "./Paragraph";
import HeaderBlock from "./HeaderBlock";

import wordList from "../wordsList";

const App = () => (
    <>
        <HeaderBlock>
            <Header>Время учить слова онлайн!</Header>
            <Paragraph>
                Используйте карточки для запоминания и пополняйте словарный запас
            </Paragraph>
        </HeaderBlock>
        <HeaderBlock hideBackground>
            <Header>Проверь себя</Header>
            <Paragraph>
                Отметьте слова, которые уже знаете
            </Paragraph>
            <CardList words={ wordList }/>
        </HeaderBlock>
        <HeaderBlock>
            <Header>
                Сделано с помощью
                <br/>
                React
            </Header>
            <Logo/>
        </HeaderBlock>
    </>
);

export default App;
