import React from "react";

import Card from "../Card";

import s from "./style.module.css";

const CardList = ({ words = [] }) => (
    <div className={ s.cards }>
        {
            words.map(({ eng, rus }, i) =>
                <Card
                    key={ `word-card_${ eng }` }
                    eng={ eng }
                    rus={ rus }
                />
            )
        }
    </div>
);

export default CardList;