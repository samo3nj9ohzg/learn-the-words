import React from "react";
import s from "./style.module.css";

const Paragraph = ({ children }) => <p className={ s.description }> { children }</p>;

export default Paragraph;