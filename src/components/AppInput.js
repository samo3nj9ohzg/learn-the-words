import React from "react";

const AppInput = () => (
    <label>
        <input placeholder="Search..."/>
    </label>
);

export default AppInput;