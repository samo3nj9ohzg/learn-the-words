import React from "react";
import s from "./style.module.css";

const Header = ({ children }) => <h1 className={ s.header }>{ children }</h1>;

export default Header;