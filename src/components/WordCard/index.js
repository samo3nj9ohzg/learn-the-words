import React from "react";

import s from "./style.module.css";

const WordCard = ({ text }) => {
    const hue = Math.random() * 360;
    const css = {
        background: `hsl(${ hue }, 60%, 40%)`
    };
    return (
        <div style={ css } className={ s.wordCard }>
            <h2>{ text }</h2>
        </div>
    );
};

export default WordCard;