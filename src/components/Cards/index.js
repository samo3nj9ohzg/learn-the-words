import React from "react";

import WordCard from "../WordCard";

import s from "./style.module.css";

const Cards = ({ cards = [] }) => (
    <div className={ s.cards }>
        {
            cards.map(v =>
                <WordCard
                    key={ `eng-card_${ v }` }
                    text={ v }
                />
            )
        }
    </div>
);

export default Cards;