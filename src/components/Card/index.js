import React, { Component } from "react";

import ncn from "../../noclassnames";

import s from "./Card.module.scss";

class Card extends Component {
    state = {
        isLearned: false
    };

    render() {
        const { eng, rus } = this.props;
        const { isLearned } = this.state;

        return (
            <div
                className={ ncn(s.card, isLearned && s.learned) }
                onClick={ this.onClickHandler }
            >
                <div className={ s.cardInner }>
                    <div className={ s.cardFront }>
                        { eng }
                    </div>
                    <div className={ s.cardBack }>
                        { rus }
                    </div>
                </div>
            </div>
        );
    }

    onClickHandler = () => {
        let { isLearned } = this.state;

        this.setState({ isLearned: !isLearned });
    };
}

export default Card;