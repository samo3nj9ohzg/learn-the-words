import React from "react";
import AppInput from "../AppInput";

import s from "./style.module.css";

const AppList = () => {
    const items = ["Item 1", "Item 2"];

    return (
        <ul>
            <AppInput/>
            { items.map(v => <li className={ s.listItem }>{ v }</li>) }
        </ul>
    );
};

export default AppList;