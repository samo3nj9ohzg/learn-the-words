import React from "react";

const AppHeader = () => {
    const css = {
        color: `hsl(${ Math.random() * 360 }, 70%, 70%)`
    };
    return <h1 style={ css }>Hello World, React.js!</h1>;
};

export default AppHeader;